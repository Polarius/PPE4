package com.example.amunoz.shinsekaiyori;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.location.Address;
import android.location.Geocoder;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.amunoz.shinsekaiyori.POJO.Place;

import java.io.IOException;
import java.util.List;
import java.util.Locale;

/**
 * Created by amunoz on 18/04/2018.
 */

public class EditPlaceActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {

    private Toolbar toolbar;
    private DrawerLayout drawer;
    private Context context;

    private EditText title;
    private EditText desc;
    private EditText shortDesc;
    private EditText numRue;
    private EditText nomRue;
    private EditText CP;
    private EditText city;
    private EditText country;
    private TextView pageTitle;
    private Button submit;

    public double lati;
    public double longi;

    private NavigationView navigationView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_place);
        toolbar = findViewById(R.id.toolbar);
        drawer = findViewById(R.id.drawer_layout);
        context = getApplicationContext();
        drawer.setScrimColor(getResources().getColor(android.R.color.transparent));
        setSupportActionBar(toolbar);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, drawer, toolbar, R.string.drawer_open, R.string.drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();
        navigationView = findViewById(R.id.navigation);


        title = findViewById(R.id.edit_title);
        desc = findViewById(R.id.edit_desc);
        shortDesc = findViewById(R.id.edit_shortDesc);
        numRue = findViewById(R.id.edit_numRue);
        nomRue = findViewById(R.id.edit_nomRue);
        CP = findViewById(R.id.edit_CP);
        city = findViewById(R.id.edit_ville);
        country = findViewById(R.id.edit_country);

        final Place place = (Place) getIntent().getSerializableExtra("placeToBe");
        pageTitle = findViewById(R.id.edit_place_title);
        submit = findViewById(R.id.edit_place_submit);

        if (place == null) {

            pageTitle.setText(getResources().getText(R.string.edit_place_pageTitle_new));
            submit.setText(getResources().getText(R.string.valider));
            submit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    createPlace();
                }
            });
        } else {
            submit.setText(getResources().getText(R.string.modifier));
            submit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    editPlace(place);
                }
            });
            pageTitle.setText(getResources().getText(R.string.edit_place_pageTitle_edit));
            title.setText(place.title);
            desc.setText(place.desc);
            shortDesc.setText(place.shortDesc);
            numRue.setText(String.valueOf(place.numRue));
            nomRue.setText(place.nomRue);
            CP.setText(place.CP);
            city.setText(place.ville);
            country.setText(place.pays);
        }

        navigationView.setNavigationItemSelectedListener(this);
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        Intent intentHome = new Intent(EditPlaceActivity.this, MainActivity.class);
        Intent intentMap = new Intent(EditPlaceActivity.this, MapActivity.class);
        Intent intentListPlace = new Intent(EditPlaceActivity.this, ListPlaceActivity.class);
        Intent intentListCatPlace = new Intent(EditPlaceActivity.this, ListCatPlaceActivity.class);
        Intent intentListEvent = new Intent(EditPlaceActivity.this, ListEventActivity.class);

        switch (item.getItemId()) {
            case R.id.menu_home:
                EditPlaceActivity.this.startActivity(intentHome);
                return true;
            case R.id.menu_map:
                EditPlaceActivity.this.startActivity(intentMap);
                return true;
            case R.id.menu_list_place:
                EditPlaceActivity.this.startActivity(intentListPlace);
                return true;
            case R.id.menu_list_event:
                EditPlaceActivity.this.startActivity(intentListEvent);
                return true;
        }

        return super.onOptionsItemSelected(item);

    }

    public void editPlace(Place place) {
        if(TextUtils.isEmpty(title.getText().toString())) {
            Toast.makeText(context, "Veuillez renseigner tout les champs", Toast.LENGTH_SHORT).show();
            return;
        }
        if(TextUtils.isEmpty(desc.getText().toString())) {
            Toast.makeText(context, "Veuillez renseigner tout les champs", Toast.LENGTH_SHORT).show();
            return;
        }
        if(TextUtils.isEmpty(shortDesc.getText().toString())) {
            Toast.makeText(context, "Veuillez renseigner tout les champs", Toast.LENGTH_SHORT).show();
            return;
        }
        if(TextUtils.isEmpty(numRue.getText().toString())) {
            Toast.makeText(context, "Veuillez renseigner tout les champs", Toast.LENGTH_SHORT).show();
            return;
        }
        if(TextUtils.isEmpty(nomRue.getText().toString())) {
            Toast.makeText(context, "Veuillez renseigner tout les champs", Toast.LENGTH_SHORT).show();
            return;
        }
        if(TextUtils.isEmpty(CP.getText().toString())) {
            Toast.makeText(context, "Veuillez renseigner tout les champs", Toast.LENGTH_SHORT).show();
            return;
        }
        if(TextUtils.isEmpty(city.getText().toString())) {
            Toast.makeText(context, "Veuillez renseigner tout les champs", Toast.LENGTH_SHORT).show();
            return;
        }
        if(TextUtils.isEmpty(country.getText().toString())) {
            Toast.makeText(context, "Veuillez renseigner tout les champs", Toast.LENGTH_SHORT).show();
            return;
        }

        /*Geocoder geo = new Geocoder(context, Locale.FRENCH);
        String adr = Uri.parse(numRue.getText().toString() + " " + nomRue.getText().toString() + " " + CP.getText().toString() + " " + city.getText().toString()).toString();

        try {
            List<Address> addList = geo.getFromLocationName(adr,5);

            if(!addList.isEmpty()) {
                Address a = addList.get(0);
                lati = a.getLatitude();
                longi = a.getLongitude();
            }

        }catch(IOException e) {

        }*/

        place.title = title.getText().toString();
        place.desc = desc.getText().toString();
        place.shortDesc = shortDesc.getText().toString();
        place.numRue = Integer.parseInt(numRue.getText().toString());
        place.nomRue = nomRue.getText().toString();
        place.CP = CP.getText().toString();
        place.ville = city.getText().toString();
        place.pays = country.getText().toString();
        //place.lati = lati;
        //place.longi = longi;

        place.updatePlace(context);

        Intent intentListPlace = new Intent(EditPlaceActivity.this, ListPlaceActivity.class);
        EditPlaceActivity.this.startActivity(intentListPlace);
    }

    public void createPlace() {
        if(TextUtils.isEmpty(title.getText().toString())) {
            Toast.makeText(context, "Veuillez renseigner tout les champs", Toast.LENGTH_SHORT).show();
            return;
        }
        if(TextUtils.isEmpty(desc.getText().toString())) {
            Toast.makeText(context, "Veuillez renseigner tout les champs", Toast.LENGTH_SHORT).show();
            return;
        }
        if(TextUtils.isEmpty(shortDesc.getText().toString())) {
            Toast.makeText(context, "Veuillez renseigner tout les champs", Toast.LENGTH_SHORT).show();
            return;
        }
        if(TextUtils.isEmpty(numRue.getText().toString())) {
            Toast.makeText(context, "Veuillez renseigner tout les champs", Toast.LENGTH_SHORT).show();
            return;
        }
        if(TextUtils.isEmpty(nomRue.getText().toString())) {
            Toast.makeText(context, "Veuillez renseigner tout les champs", Toast.LENGTH_SHORT).show();
            return;
        }
        if(TextUtils.isEmpty(CP.getText().toString())) {
            Toast.makeText(context, "Veuillez renseigner tout les champs", Toast.LENGTH_SHORT).show();
            return;
        }
        if(TextUtils.isEmpty(city.getText().toString())) {
            Toast.makeText(context, "Veuillez renseigner tout les champs", Toast.LENGTH_SHORT).show();
            return;
        }
        if(TextUtils.isEmpty(country.getText().toString())) {
            Toast.makeText(context, "Veuillez renseigner tout les champs", Toast.LENGTH_SHORT).show();
            return;
        }

        /*Geocoder geo = new Geocoder(context, Locale.FRENCH);
        String adr = Uri.parse(numRue.getText().toString() + " " + nomRue.getText().toString() + " " + CP.getText().toString() + " " + city.getText().toString()).toString();

        try {
            List<Address> addList = geo.getFromLocationName(adr, 5);
            if (!addList.isEmpty()) {
                Address a = addList.get(0);
                lati = a.getLatitude();
                longi = a.getLongitude();
            }
        } catch (IOException e) {

        }*/

        Place newPlace = new Place(title.getText().toString(), desc.getText().toString(), shortDesc.getText().toString(), Integer.parseInt(numRue.getText().toString()), nomRue.getText().toString(), CP.getText().toString(), city.getText().toString(), country.getText().toString(), 0);
        newPlace.addPlace(context);
        Intent intentListPlace = new Intent(EditPlaceActivity.this, ListPlaceActivity.class);
        EditPlaceActivity.this.startActivity(intentListPlace);
    }

}

