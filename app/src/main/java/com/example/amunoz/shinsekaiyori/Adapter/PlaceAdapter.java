package com.example.amunoz.shinsekaiyori.Adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.TextureView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.example.amunoz.shinsekaiyori.EditPlaceActivity;
import com.example.amunoz.shinsekaiyori.ListPlaceActivity;
import com.example.amunoz.shinsekaiyori.POJO.Place;
import com.example.amunoz.shinsekaiyori.R;

import org.w3c.dom.Text;

import java.util.ArrayList;

/**
 * Created by amunoz on 20/03/2018.
 */

public class PlaceAdapter extends RecyclerView.Adapter<PlaceAdapter.ViewHolder> {

    private ArrayList<Place> mDataSet;
    public static View placeView;
    private final PlaceAdapter.onItemClickListener listener;

    public interface onItemClickListener {
        void onItemClick(Place item);
    }

    public PlaceAdapter(ArrayList<Place> list, PlaceAdapter.onItemClickListener listener) {
        this.mDataSet = list;
        this.listener = listener;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        public TextView mTitle;
        public TextView mShortDesc;
        public TextView mAdress;
        public View v;

        public ViewHolder(View itemView) {
            super(itemView);
            v = itemView;
            mTitle = itemView.findViewById(R.id.place_title);
            mShortDesc = itemView.findViewById(R.id.place_desc);
            mAdress = itemView.findViewById(R.id.place_adress);
        }

        public void bind(final Place item, final PlaceAdapter.onItemClickListener listener) {
            placeView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.onItemClick(item);
                }
            });
        }
    }

    @Override
    public PlaceAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        Context context = parent.getContext();
        LayoutInflater inflater = LayoutInflater.from(context);
        placeView = inflater.inflate(R.layout.list_item_place, parent, false);
        ViewHolder vh = new ViewHolder(placeView);
        return vh;
    }

    @Override
    public void onBindViewHolder(PlaceAdapter.ViewHolder viewHolder, final int position) {
        final Place pl = mDataSet.get(position);
        TextView tv = viewHolder.mTitle;
        tv.setText(pl.title);
        TextView tv2 = viewHolder.mShortDesc;
        tv2.setText(pl.shortDesc);
        TextView tv3 = viewHolder.mAdress;
        tv3.setText(pl.numRue + " " + pl.nomRue + " " + pl.ville + " (" + pl.CP + ") ");
        viewHolder.bind(pl,listener);

        viewHolder.v.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                mDataSet.remove(position);
                pl.deletePlace(v.getContext());
                notifyItemRemoved(position);
                notifyDataSetChanged();
                return true;
            }
        });

    }

    @Override
    public int getItemCount() {
        return mDataSet.size();
    }








}
