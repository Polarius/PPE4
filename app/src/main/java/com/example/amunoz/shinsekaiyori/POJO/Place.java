package com.example.amunoz.shinsekaiyori.POJO;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.location.Geocoder;

import com.example.amunoz.shinsekaiyori.Database.LocalSQLiteOpenHelper;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Locale;

/**
 * Created by amunoz on 20/03/2018.
 */

public class Place implements Serializable {

    public static final String tbname = "place";

    public int id;
    public String title;
    public String desc;
    public String shortDesc;
    public int numRue;
    public String nomRue;
    public String CP;
    public String ville;
    public String pays;
    public int cat;

    public Place(String title, String desc, String shortDesc, int numRue, String nomRue, String CP, String ville, String pays, int cat) {
        this.title = title;
        this.desc = desc;
        this.shortDesc = shortDesc;
        this.numRue = numRue;
        this.nomRue = nomRue;
        this.CP = CP;
        this.ville = ville;
        this.pays = pays;
        this.cat = cat;
    }

    public Place(Context context, Cursor cursor) {
        this.id = cursor.getInt(cursor.getColumnIndex("id"));
        this.title = cursor.getString(cursor.getColumnIndex("title"));
        this.desc = cursor.getString(cursor.getColumnIndex("desc"));
        this.shortDesc = cursor.getString(cursor.getColumnIndex("shortDesc"));
        this.numRue = cursor.getInt(cursor.getColumnIndex("numRue"));
        this.nomRue = cursor.getString(cursor.getColumnIndex("nomRue"));
        this.CP = cursor.getString(cursor.getColumnIndex("CP"));
        this.ville = cursor.getString(cursor.getColumnIndex("ville"));
        this.pays = cursor.getString(cursor.getColumnIndex("pays"));
        this.cat = cursor.getInt(cursor.getColumnIndex("cat"));
    }

    public Place(Context context, int id) {
        LocalSQLiteOpenHelper helper = new LocalSQLiteOpenHelper(context);
        SQLiteDatabase db = helper.getReadableDatabase();
        Cursor cursor = db.query(true,tbname,new String[]{"id","title","desc","shortDesc","numRue","nomRue","CP","ville","pays","cat"},"id",new String[]{String.valueOf(id)},null,null,null,null);
        cursor.moveToNext();
        this.id = cursor.getInt(cursor.getColumnIndex("id"));
        this.title = cursor.getString(cursor.getColumnIndex("title"));
        this.desc = cursor.getString(cursor.getColumnIndex("desc"));
        this.shortDesc = cursor.getString(cursor.getColumnIndex("shortDesc"));
        this.numRue = cursor.getInt(cursor.getColumnIndex("numRue"));
        this.nomRue = cursor.getString(cursor.getColumnIndex("nomRue"));
        this.CP = cursor.getString(cursor.getColumnIndex("CP"));
        this.ville = cursor.getString(cursor.getColumnIndex("ville"));
        this.pays = cursor.getString(cursor.getColumnIndex("pays"));
        this.cat = cursor.getInt(cursor.getColumnIndex("cat"));
        cursor.close();
        db.close();
    }

    public void addPlace(Context context){
        ContentValues cv = new ContentValues();
        cv.put("title", this.title);
        cv.put("desc", this.desc);
        cv.put("shortDesc",this.shortDesc );
        cv.put("numRue", this.numRue);
        cv.put("nomRue", this.nomRue);
        cv.put("CP", this.CP);
        cv.put("ville", this.ville);
        cv.put("pays", this.pays);
        cv.put("cat", this.cat);
        LocalSQLiteOpenHelper helper = new LocalSQLiteOpenHelper(context);
        SQLiteDatabase db = helper.getWritableDatabase();
        db.insert(Place.tbname, null, cv);
        db.close();
    }

    public void updatePlace(Context context){
        ContentValues cv = new ContentValues();
        cv.put("title", this.title);
        cv.put("desc", this.desc);
        cv.put("shortDesc",this.shortDesc );
        cv.put("numRue", this.numRue);
        cv.put("nomRue", this.nomRue);
        cv.put("CP", this.CP);
        cv.put("ville", this.ville);
        cv.put("pays", this.pays);
        cv.put("cat", this.cat);
        LocalSQLiteOpenHelper helper = new LocalSQLiteOpenHelper(context);
        SQLiteDatabase db = helper.getWritableDatabase();
        db.update(Place.tbname, cv, "id = ?", new String[]{String.valueOf(this.id)});
        db.close();
    }

    public void deletePlace(Context context){
        LocalSQLiteOpenHelper helper = new LocalSQLiteOpenHelper(context);
        SQLiteDatabase db = helper.getWritableDatabase();
        db.delete(Place.tbname, "id = ?", new String[]{String.valueOf(this.id)});
        db.close();
    }

    public static ArrayList<Place> getPlaces(Context context){
        ArrayList<Place> e = new ArrayList<>();
        LocalSQLiteOpenHelper helper = new LocalSQLiteOpenHelper(context);
        SQLiteDatabase db = helper.getReadableDatabase();
        Cursor c = db.query(false, Place.tbname,new String[]{"id","title","desc","shortDesc","numRue","nomRue","CP","ville","pays","cat"},
                null, null, null, null, null, null);
        while(c.moveToNext()){
            e.add(new Place(context,c));
        }
        c.close();
        db.close();
        return e;
    }


}
