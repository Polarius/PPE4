package com.example.amunoz.shinsekaiyori;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.view.menu.MenuBuilder;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;

//@TODO BARRE DE RECHERCHE
//@TODO FINIR LISTES
//@TODO

import com.example.amunoz.shinsekaiyori.POJO.Event;
import com.synnapps.carouselview.CarouselView;
import com.synnapps.carouselview.ImageListener;

public class MainActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {

    private Toolbar toolbar;
    private DrawerLayout drawer;
    private Context context;
    private NavigationView navigationView;

    CarouselView carouselView;

    int[] sampleImages = {R.drawable.banner1, R.drawable.banner2, R.drawable.banner_img};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        toolbar             = findViewById(R.id.toolbar);
        drawer              = findViewById(R.id.drawer_layout);
        context             = getApplicationContext();
        navigationView      = findViewById(R.id.navigation);
        ImageButton fb      = findViewById(R.id.socialFB);
        ImageButton web     = findViewById(R.id.socialWB);

        drawer.setScrimColor(getResources().getColor(android.R.color.transparent));

        setSupportActionBar(toolbar);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, drawer, toolbar, R.string.drawer_open, R.string.drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();
        navigationView.setNavigationItemSelectedListener(this);

        carouselView = (CarouselView) findViewById(R.id.carouselView);
        carouselView.setPageCount(sampleImages.length);
        carouselView.setImageListener(imageListener);

        fb.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(newFacebookIntent(context.getPackageManager(), "https://www.facebook.com/associationkibonoyume/"));
            }
        });

        web.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(Intent.ACTION_VIEW,Uri.parse("https://kibonoyume.jimdo.com/")));
            }
        });

    }

    ImageListener imageListener = new ImageListener() {
        @Override
        public void setImageForPosition(int position, ImageView imageView) {
            imageView.setImageResource(sampleImages[position]);
        }
    };

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        Intent intentHome = new Intent(MainActivity.this, MainActivity.class);
        Intent intentMap = new Intent(MainActivity.this, MapActivity.class);
        Intent intentListPlace = new Intent(MainActivity.this, ListPlaceActivity.class);
        Intent intentListEvent = new Intent(MainActivity.this, ListEventActivity.class);

        switch (item.getItemId()) {
            case R.id.menu_home:
                MainActivity.this.startActivity(intentHome);
                return true;
            case R.id.menu_map:
                MainActivity.this.startActivity(intentMap);
                return true;
            case R.id.menu_list_place:
                MainActivity.this.startActivity(intentListPlace);
                return true;
            case R.id.menu_list_event:
                MainActivity.this.startActivity(intentListEvent);
                return true;
        }

        return super.onOptionsItemSelected(item);

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    public static Intent newFacebookIntent(PackageManager pm, String url) {
        Uri uri = Uri.parse(url);
        try {
            ApplicationInfo applicationInfo = pm.getApplicationInfo("com.facebook.katana", 0);
            if (applicationInfo.enabled) {
                uri = Uri.parse("fb://facewebmodal/f?href=" + url);
            }
        } catch (PackageManager.NameNotFoundException ignored) {
            Log.e("ERREUR - ", ignored.getMessage());
        }
        return new Intent(Intent.ACTION_VIEW, uri);
    }

}
