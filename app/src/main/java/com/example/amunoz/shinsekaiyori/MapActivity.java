package com.example.amunoz.shinsekaiyori;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.view.menu.MenuBuilder;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;

public class MapActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {

    private Toolbar toolbar;
    private DrawerLayout drawer;
    private Context context;
    private NavigationView navigationView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_map);

        toolbar = findViewById(R.id.toolbar);
        drawer = findViewById(R.id.drawer_layout);
        context = getApplicationContext();
        navigationView = findViewById(R.id.navigation);

        drawer.setScrimColor(getResources().getColor(android.R.color.transparent));

        setSupportActionBar(toolbar);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, drawer, toolbar, R.string.drawer_open, R.string.drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();
        navigationView.setNavigationItemSelectedListener(this);

       //CharSequence text = "Map";
       //int duration = Toast.LENGTH_LONG;
       //Toast toast = Toast.makeText(context, text, duration);
       //toast.show();
    }
/*
    public void openFragment(Fragment fragment, String tag) {
        FragmentManager fragmentManager = this.getSupportFragmentManager();
        FragmentTransaction transaction = fragmentManager.beginTransaction();
        transaction.replace(R.id.mapFragment_container,fragment,tag);
        transaction.addToBackStack(null);
        transaction.commit();
    }*/

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {

        Intent intentHome = new Intent(MapActivity.this, MainActivity.class);
        Intent intentMap = new Intent(MapActivity.this, MapActivity.class);
        Intent intentListPlace = new Intent(MapActivity.this, ListPlaceActivity.class);
        Intent intentListCatPlace = new Intent(MapActivity.this, ListCatPlaceActivity.class);
        Intent intentListEvent = new Intent(MapActivity.this, ListEventActivity.class);

        switch (item.getItemId()) {
            case R.id.menu_home:
                MapActivity.this.startActivity(intentHome);
                return true;
            case R.id.menu_map:
                MapActivity.this.startActivity(intentMap);
                return true;
            case R.id.menu_list_place:
                MapActivity.this.startActivity(intentListPlace);
                return true;
            case R.id.menu_list_event:
                MapActivity.this.startActivity(intentListEvent);
                return true;
        }

        return super.onOptionsItemSelected(item);

    }
}
