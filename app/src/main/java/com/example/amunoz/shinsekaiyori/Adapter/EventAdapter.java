package com.example.amunoz.shinsekaiyori.Adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.TextureView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.amunoz.shinsekaiyori.POJO.Event;
import com.example.amunoz.shinsekaiyori.R;

import org.w3c.dom.Text;

import java.util.ArrayList;

/**
 * Created by amunoz on 20/03/2018.
 */

public class EventAdapter extends RecyclerView.Adapter<EventAdapter.ViewHolder> {

    private ArrayList<Event> mDataSet;
    public static View eventView;
    private final EventAdapter.onItemClickListener listener;

    public interface onItemClickListener {
        void onItemClick(Event item);
    }

    public EventAdapter(ArrayList<Event> list, EventAdapter.onItemClickListener listener) {
        this.mDataSet = list;
        this.listener = listener;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        public TextView mTitle;
        public TextView mShortDesc;
        public TextView mAdress;
        public View v;


        public ViewHolder(View itemView) {
            super(itemView);
            v = itemView;
            mTitle = itemView.findViewById(R.id.event_title);
            mShortDesc = itemView.findViewById(R.id.event_desc);
            mAdress = itemView.findViewById(R.id.event_date);
        }

        public void bind(final Event item, final EventAdapter.onItemClickListener listener) {
            eventView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.onItemClick(item);
                }
            });
        }
    }

    @Override
    public EventAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        Context context = parent.getContext();
        LayoutInflater inflater = LayoutInflater.from(context);
        eventView = inflater.inflate(R.layout.list_item_event, parent, false);
        ViewHolder vh = new ViewHolder(eventView);
        return vh;
    }

    @Override
    public void onBindViewHolder(EventAdapter.ViewHolder viewHolder, final int position) {
        final Event ev = mDataSet.get(position);
        TextView tv = viewHolder.mTitle;
        tv.setText(ev.title);
        TextView tv2 = viewHolder.mShortDesc;
        tv2.setText(ev.shortDesc);
        TextView tv3 = viewHolder.mAdress;
        tv3.setText(String.valueOf(ev.timestamp_start));
        viewHolder.bind(ev,listener);

        viewHolder.v.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                mDataSet.remove(position);
                ev.deleteEvent(v.getContext());
                notifyItemRemoved(position);
                notifyDataSetChanged();
                return true;
            }
        });

    }

    @Override
    public int getItemCount() {
        return mDataSet.size();
    }

}
