package com.example.amunoz.shinsekaiyori.Database;

/**
 * Created by amunoz on 20/03/2018.
 */


import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;


public class LocalSQLiteOpenHelper extends SQLiteOpenHelper {

    static String DB_NAME = "shinsekai";
    static int DB_VERSION = 4;

    public LocalSQLiteOpenHelper(Context context) {
        super(context, DB_NAME, null, DB_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String createPlaceTable = "CREATE TABLE place(id INTEGER PRIMARY KEY, title TEXT, desc TEXT, shortDesc TEXT, numRue INTEGER, nomRue TEXT, CP TEXT, ville TEXT, pays TEXT, cat INTEGER);";
        String createEventTable = "CREATE TABLE event(id INTEGER PRIMARY KEY, title TEXT, desc TEXT, shortDesc TEXT, lieu TEXT, timestamp_start INTEGER, timestamp_end INTEGER, duration INTEGER, price REAL);";
        String createCatTable = "CREATE TABLE cat(id INTEGER PRIMARY KEY, lib TEXT);";
        db.execSQL(createPlaceTable);
        db.execSQL(createEventTable);
        db.execSQL(createCatTable);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        for (int i = oldVersion; i < newVersion; i++) {
            int versionToUpdate = i + 1;
            if (versionToUpdate == 2) {
                //CALL METHOD TO UPDATE BDD
            }
        }
    }
}
