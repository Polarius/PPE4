package com.example.amunoz.shinsekaiyori.POJO;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.example.amunoz.shinsekaiyori.Database.LocalSQLiteOpenHelper;

import java.io.Serializable;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.util.ArrayList;

/**
 * Created by amunoz on 20/03/2018.
 */

public class Event implements Serializable {
    public static final String tbname = "event";

    public int id;
    public String title;
    public String shortDesc;
    public String desc;
    public int lieu;
    public String timestamp_start;
    public String timestamp_end;
    public int duration;
    public double price;

    public Event(String title, String shortDesc, String desc, int lieu, String timestamp_start, String timestamp_end, int duration, double price) {
        this.title = title;
        this.desc = desc;
        this.shortDesc = shortDesc;
        this.lieu = lieu;
        this.timestamp_start = timestamp_start;
        this.timestamp_end = timestamp_end;
        this.duration = duration;
        this.price = price;
    }

    public Event(Context context, Cursor cursor) {
        this.id = cursor.getInt(cursor.getColumnIndex("id"));
        this.title = cursor.getString(cursor.getColumnIndex("title"));
        this.desc = cursor.getString(cursor.getColumnIndex("desc"));
        this.shortDesc = cursor.getString(cursor.getColumnIndex("shortDesc"));
        this.timestamp_start = cursor.getString(cursor.getColumnIndex("timestamp_start"));
        this.timestamp_end = cursor.getString(cursor.getColumnIndex("timestamp_end"));
        this.duration = cursor.getInt(cursor.getColumnIndex("duration"));
        this.price = cursor.getDouble(cursor.getColumnIndex("price"));
        this.lieu = cursor.getInt(cursor.getColumnIndex("lieu"));
    }

    public Event(Context context, int id) {
        LocalSQLiteOpenHelper helper = new LocalSQLiteOpenHelper(context);
        SQLiteDatabase db = helper.getReadableDatabase();
        Cursor cursor = db.query(true,tbname,new String[]{"id","title","desc","shortDesc","lieu","timestamp_start","timestamp_end","duration","price"},"id",new String[]{String.valueOf(id)},null,null,null,null);
        cursor.moveToNext();
        this.id = cursor.getInt(cursor.getColumnIndex("id"));
        this.title = cursor.getString(cursor.getColumnIndex("title"));
        this.desc = cursor.getString(cursor.getColumnIndex("desc"));
        this.shortDesc = cursor.getString(cursor.getColumnIndex("shortDesc"));
        this.timestamp_start = cursor.getString(cursor.getColumnIndex("timestamp_start"));
        this.timestamp_end = cursor.getString(cursor.getColumnIndex("timestamp_end"));
        this.duration = cursor.getInt(cursor.getColumnIndex("duration"));
        this.price = cursor.getDouble(cursor.getColumnIndex("price"));
        this.lieu = cursor.getInt(cursor.getColumnIndex("lieu"));
        cursor.close();
        db.close();
    }

    public void addEvent(Context context){
        ContentValues cv = new ContentValues();
        cv.put("title", this.title);
        cv.put("desc", this.desc);
        cv.put("shortDesc",this.shortDesc );
        cv.put("timestamp_start", this.timestamp_start);
        cv.put("timestamp_end", this.timestamp_end);
        cv.put("duration", this.duration);
        cv.put("price", this.price);
        cv.put("lieu", this.lieu);
        LocalSQLiteOpenHelper helper = new LocalSQLiteOpenHelper(context);
        SQLiteDatabase db = helper.getWritableDatabase();
        db.insert(Event.tbname, null, cv);
        db.close();
    }

    public void updateEvent(Context context){
        ContentValues cv = new ContentValues();
        cv.put("title", this.title);
        cv.put("desc", this.desc);
        cv.put("shortDesc",this.shortDesc );
        cv.put("timestamp_start", this.timestamp_start);
        cv.put("timestamp_end", this.timestamp_end);
        cv.put("duration", this.duration);
        cv.put("price", this.price);
        cv.put("lieu", this.lieu);
        LocalSQLiteOpenHelper helper = new LocalSQLiteOpenHelper(context);
        SQLiteDatabase db = helper.getWritableDatabase();
        db.update(Event.tbname, cv, "id = ?", new String[]{String.valueOf(this.id)});
        db.close();
    }

    public void deleteEvent(Context context){
        LocalSQLiteOpenHelper helper = new LocalSQLiteOpenHelper(context);
        SQLiteDatabase db = helper.getWritableDatabase();
        db.delete(Event.tbname, "id = ?", new String[]{String.valueOf(this.id)});
        db.close();
    }

    public static ArrayList<Event> getEvents(Context context){
        ArrayList<Event> e = new ArrayList<>();
        LocalSQLiteOpenHelper helper = new LocalSQLiteOpenHelper(context);
        SQLiteDatabase db = helper.getReadableDatabase();
        Cursor c = db.query(true, Event.tbname,new String[]{"id","title","desc","shortDesc","lieu","timestamp_start","timestamp_end","duration","price"},
                null, null, null, null, "timestamp_start", null);
        while(c.moveToNext()){
            e.add(new Event(context,c));
        }
        c.close();
        db.close();
        return e;
    }

}
