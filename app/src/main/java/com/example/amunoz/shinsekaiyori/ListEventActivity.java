package com.example.amunoz.shinsekaiyori;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.view.menu.MenuBuilder;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.example.amunoz.shinsekaiyori.Adapter.EventAdapter;
import com.example.amunoz.shinsekaiyori.POJO.Event;

import java.io.Serializable;
import java.util.ArrayList;

public class ListEventActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {

    private Toolbar toolbar;
    private DrawerLayout drawer;
    private Context context;
    private NavigationView navigationView;
    private RecyclerView list;
    private EventAdapter adapter;
    private RecyclerView.LayoutManager layoutManager;
    private ImageButton btn;
    public final static String TAG = "ListEvents";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_event);

        toolbar = findViewById(R.id.toolbar);
        drawer = findViewById(R.id.drawer_layout);
        context = getApplicationContext();
        navigationView = findViewById(R.id.navigation);

        drawer.setScrimColor(getResources().getColor(android.R.color.transparent));

        ArrayList<Event> listEvents = Event.getEvents(context);
        EventAdapter eventAdapter = new EventAdapter(listEvents, new EventAdapter.onItemClickListener() {
            @Override
            public void onItemClick(Event item) {
                Intent intentEdit = new Intent(ListEventActivity.this, EditEventActivity.class);
                intentEdit.putExtra("eventToBe",(Serializable) item);
                ListEventActivity.this.startActivity(intentEdit);
            }

        });

        RecyclerView listContainer = findViewById(R.id.list_event_list);
        listContainer.setHasFixedSize(true);
        LinearLayoutManager layoutManager = new LinearLayoutManager(context);
        listContainer.setLayoutManager(layoutManager);

        setSupportActionBar(toolbar);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, drawer, toolbar, R.string.drawer_open, R.string.drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();
        navigationView.setNavigationItemSelectedListener(this);

        btn = findViewById(R.id.event_new_event);
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent goNew = new Intent(ListEventActivity.this, EditEventActivity.class);
                ListEventActivity.this.startActivity(goNew);
            }
        });

        listContainer.setAdapter(eventAdapter);

    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {

        Intent intentHome = new Intent(ListEventActivity.this, MainActivity.class);
        Intent intentMap = new Intent(ListEventActivity.this, MapActivity.class);
        Intent intentListPlace = new Intent(ListEventActivity.this, ListPlaceActivity.class);
        Intent intentListEvent = new Intent(ListEventActivity.this, ListEventActivity.class);

        switch (item.getItemId()) {
            case R.id.menu_home:
                ListEventActivity.this.startActivity(intentHome);
                return true;
            case R.id.menu_map:
                ListEventActivity.this.startActivity(intentMap);
                return true;
            case R.id.menu_list_place:
                ListEventActivity.this.startActivity(intentListPlace);
                return true;
            case R.id.menu_list_event:
                ListEventActivity.this.startActivity(intentListEvent);
                return true;
        }

        return super.onOptionsItemSelected(item);

    }
}