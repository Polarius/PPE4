package com.example.amunoz.shinsekaiyori.POJO;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.example.amunoz.shinsekaiyori.Database.LocalSQLiteOpenHelper;

import java.util.ArrayList;

/**
 * Created by amunoz on 20/03/2018.
 */

public class CatPlace {

    public static final String tbname = "catplace";

    public int id;
    public String lib;

    public CatPlace(int id, String lib) {
        this.id = id;
        this.lib = lib;
    }

    public CatPlace(Context context, Cursor cursor) {
        this.id = cursor.getInt(cursor.getColumnIndex("id"));
        this.lib = cursor.getString(cursor.getColumnIndex("lib"));
    }

    public ArrayList<CatPlace> getCats(Context context) {
        ArrayList<CatPlace> list = new ArrayList<CatPlace>();

        LocalSQLiteOpenHelper helper = new LocalSQLiteOpenHelper(context);
        SQLiteDatabase db = helper.getReadableDatabase();
        Cursor c = db.query(true, tbname,new String[]{"id","lib"},
                null, null, null, null, null, null);
        while(c.moveToNext()){
            list.add(new CatPlace(context,c));
        }
        c.close();
        db.close();

        return list;
    }

    public void addCat(Context context){
        ContentValues cv = new ContentValues();
        cv.put("lib", this.lib);
        LocalSQLiteOpenHelper helper = new LocalSQLiteOpenHelper(context);
        SQLiteDatabase db = helper.getWritableDatabase();
        db.insert(CatPlace.tbname, null, cv);
        db.close();
    }

    public void updateCat(Context context){
        ContentValues cv = new ContentValues();
        cv.put("lib", this.lib);
        LocalSQLiteOpenHelper helper = new LocalSQLiteOpenHelper(context);
        SQLiteDatabase db = helper.getWritableDatabase();
        db.update(CatPlace.tbname, cv, "id = ?", new String[]{String.valueOf(this.id)});
        db.close();
    }

    public void deleteCat(Context context){
        LocalSQLiteOpenHelper helper = new LocalSQLiteOpenHelper(context);
        SQLiteDatabase db = helper.getWritableDatabase();
        db.delete(CatPlace.tbname, "id = ?", new String[]{String.valueOf(this.id)});
        db.close();
    }



}
