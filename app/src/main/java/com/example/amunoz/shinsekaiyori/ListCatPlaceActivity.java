package com.example.amunoz.shinsekaiyori;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.view.menu.MenuBuilder;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.Toast;

public class ListCatPlaceActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {

    private Toolbar toolbar;
    private DrawerLayout drawer;
    private Context context;
    private NavigationView navigationView;
    private RecyclerView list;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_place);

        toolbar = findViewById(R.id.toolbar);
        drawer = findViewById(R.id.drawer_layout);
        context = getApplicationContext();
        navigationView = findViewById(R.id.navigation);

        drawer.setScrimColor(getResources().getColor(android.R.color.transparent));

        setSupportActionBar(toolbar);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, drawer, toolbar, R.string.drawer_open, R.string.drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();
        navigationView.setNavigationItemSelectedListener(this);

    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        Intent intentHome = new Intent(ListCatPlaceActivity.this, MainActivity.class);
        Intent intentMap = new Intent(ListCatPlaceActivity.this, MapActivity.class);
        Intent intentListPlace = new Intent(ListCatPlaceActivity.this, ListPlaceActivity.class);
        Intent intentListCatPlace = new Intent(ListCatPlaceActivity.this, ListCatPlaceActivity.class);
        Intent intentListEvent = new Intent(ListCatPlaceActivity.this, ListEventActivity.class);

        switch (item.getItemId()) {
            case R.id.menu_home:
                ListCatPlaceActivity.this.startActivity(intentHome);
                return true;
            case R.id.menu_map:
                ListCatPlaceActivity.this.startActivity(intentMap);
                return true;
            case R.id.menu_list_place:
                ListCatPlaceActivity.this.startActivity(intentListPlace);
                return true;
            case R.id.menu_list_event:
                ListCatPlaceActivity.this.startActivity(intentListEvent);
                return true;
        }

        return super.onOptionsItemSelected(item);

    }

   //public void validateForm() {
   //    EditText title = (EditText) findViewById(R.id.event_title);
   //}
}