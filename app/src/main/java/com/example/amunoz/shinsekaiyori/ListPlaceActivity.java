package com.example.amunoz.shinsekaiyori;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.media.Image;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.view.menu.MenuBuilder;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.example.amunoz.shinsekaiyori.Adapter.PlaceAdapter;
import com.example.amunoz.shinsekaiyori.POJO.Place;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class ListPlaceActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {

    private Toolbar toolbar;
    private DrawerLayout drawer;
    private Context context;
    private NavigationView navigationView;
    private RecyclerView list;
    private PlaceAdapter adapter;
    private RecyclerView.LayoutManager layoutManager;
    private ImageButton btn;
    public final static String TAG = "ListPlaces";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_place);

        toolbar = findViewById(R.id.toolbar);
        drawer = findViewById(R.id.drawer_layout);
        context = getApplicationContext();
        navigationView = findViewById(R.id.navigation);

        drawer.setScrimColor(getResources().getColor(android.R.color.transparent));

        ArrayList<Place> list = Place.getPlaces(context);
        RecyclerView listContainer = findViewById(R.id.list_place_list);
        listContainer.setHasFixedSize(true);
        LinearLayoutManager layoutManager = new LinearLayoutManager(context);
        listContainer.setLayoutManager(layoutManager);

        PlaceAdapter placeAdapter = new PlaceAdapter(list, new PlaceAdapter.onItemClickListener() {
            @Override
            public void onItemClick(Place item) {
                Intent intentEdit = new Intent(ListPlaceActivity.this, EditPlaceActivity.class);
                intentEdit.putExtra("placeToBe",(Serializable) item);
                ListPlaceActivity.this.startActivity(intentEdit);
            }
        });

        listContainer.setAdapter(placeAdapter);

        setSupportActionBar(toolbar);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, drawer, toolbar, R.string.drawer_open, R.string.drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();
        navigationView.setNavigationItemSelectedListener(this);

        btn = findViewById(R.id.place_new_place_btn);
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent goNew = new Intent(ListPlaceActivity.this, EditPlaceActivity.class);
                ListPlaceActivity.this.startActivity(goNew);
            }
        });
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {

        Intent intentHome = new Intent(ListPlaceActivity.this, MainActivity.class);
        Intent intentMap = new Intent(ListPlaceActivity.this, MapActivity.class);
        Intent intentListPlace = new Intent(ListPlaceActivity.this, ListPlaceActivity.class);
        Intent intentListEvent = new Intent(ListPlaceActivity.this, ListEventActivity.class);

        switch (item.getItemId()) {
            case R.id.menu_home:
                ListPlaceActivity.this.startActivity(intentHome);
                return true;
            case R.id.menu_map:
                ListPlaceActivity.this.startActivity(intentMap);
                return true;
            case R.id.menu_list_place:
                ListPlaceActivity.this.startActivity(intentListPlace);
                return true;
            case R.id.menu_list_event:
                ListPlaceActivity.this.startActivity(intentListEvent);
                return true;
        }

        return super.onOptionsItemSelected(item);

    }


}