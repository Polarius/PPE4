package com.example.amunoz.shinsekaiyori;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.amunoz.shinsekaiyori.POJO.Event;

public class EditEventActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {

    private Toolbar toolbar;
    private DrawerLayout drawer;
    private Context context;
    private NavigationView navigationView;

    private EditText title;
    private EditText shortDesc;
    private EditText desc;
    private EditText dateStart;
    private EditText dateEnd;
    private EditText duree;
    private EditText price;

    private TextView pageTitle;
    private Button submit;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_event);
        toolbar = findViewById(R.id.toolbar);
        drawer = findViewById(R.id.drawer_layout);
        context = getApplicationContext();
        drawer.setScrimColor(getResources().getColor(android.R.color.transparent));
        setSupportActionBar(toolbar);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, drawer, toolbar, R.string.drawer_open, R.string.drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();
        navigationView = findViewById(R.id.navigation);

        // FINDVIEWBYID
        title = findViewById(R.id.edit_event_title);
        desc = findViewById(R.id.edit_event_desc);
        shortDesc = findViewById(R.id.edit_event_shortDesc);
        dateStart = findViewById(R.id.edit_event_dateStart);
        dateEnd = findViewById(R.id.edit_event_dateEnd);
        duree = findViewById(R.id.edit_event_duree);
        price = findViewById(R.id.edit_event_price);

        pageTitle = findViewById(R.id.edit_event_pageTitle);
        submit = findViewById(R.id.edit_event_submit);

        final Event event = (Event) getIntent().getSerializableExtra("eventToBe");

        if (event == null) {
            pageTitle.setText(getResources().getText(R.string.edit_event_pageTitle_new));
            submit.setText(getResources().getText(R.string.valider));
            submit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    createEvent();
                }
            });
        } else {
            submit.setText(getResources().getText(R.string.modifier));
            submit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    editEvent(event);
                }
            });
            pageTitle.setText(getResources().getText(R.string.edit_event_pageTitle_edit));

            title.setText(event.title);
            desc.setText(event.desc);
            shortDesc.setText(event.shortDesc);
            dateStart.setText(String.valueOf(event.timestamp_start));
            dateEnd.setText(String.valueOf(event.timestamp_end));
            duree.setText(String.valueOf(event.duration));
            price.setText(String.valueOf(event.price));
        }

        navigationView.setNavigationItemSelectedListener(this);
    }

    private void editEvent(Event event) {
        Toast.makeText(context, "EDIT", Toast.LENGTH_SHORT).show();

        if(TextUtils.isEmpty(title.getText().toString())) {
            Toast.makeText(context, "Veuillez renseigner tout les champs", Toast.LENGTH_SHORT).show();
            return;
        }
        if(TextUtils.isEmpty(desc.getText().toString())) {
            Toast.makeText(context, "Veuillez renseigner tout les champs", Toast.LENGTH_SHORT).show();
            return;
        }
        if(TextUtils.isEmpty(shortDesc.getText().toString())) {
            Toast.makeText(context, "Veuillez renseigner tout les champs", Toast.LENGTH_SHORT).show();
            return;
        }
        if(TextUtils.isEmpty(dateStart.getText().toString())) {
            Toast.makeText(context, "Veuillez renseigner tout les champs", Toast.LENGTH_SHORT).show();
            return;
        }
        if(TextUtils.isEmpty(dateEnd.getText().toString())) {
            Toast.makeText(context, "Veuillez renseigner tout les champs", Toast.LENGTH_SHORT).show();
            return;
        }
        if(TextUtils.isEmpty(duree.getText().toString())) {
            Toast.makeText(context, "Veuillez renseigner tout les champs", Toast.LENGTH_SHORT).show();
            return;
        }
        if(TextUtils.isEmpty(price.getText().toString())) {
            Toast.makeText(context, "Veuillez renseigner tout les champs", Toast.LENGTH_SHORT).show();
            return;
        }

        event.title = title.getText().toString();
        event.desc = desc.getText().toString();
        event.shortDesc = shortDesc.getText().toString();
        event.timestamp_start = dateStart.getText().toString();
        event.timestamp_end = dateEnd.getText().toString();
        event.duration = Integer.parseInt(duree.getText().toString());
        event.price = Double.parseDouble(price.getText().toString());

        event.updateEvent(context);

        Intent intentListPlace = new Intent(EditEventActivity.this, ListEventActivity.class);
        EditEventActivity.this.startActivity(intentListPlace);
    }

    private void createEvent() {
        if(TextUtils.isEmpty(title.getText().toString())) {
            Toast.makeText(context, "Veuillez renseigner tout les champs", Toast.LENGTH_SHORT).show();
            return;
        }
        if(TextUtils.isEmpty(desc.getText().toString())) {
            Toast.makeText(context, "Veuillez renseigner tout les champs", Toast.LENGTH_SHORT).show();
            return;
        }
        if(TextUtils.isEmpty(shortDesc.getText().toString())) {
            Toast.makeText(context, "Veuillez renseigner tout les champs", Toast.LENGTH_SHORT).show();
            return;
        }
        if(TextUtils.isEmpty(dateStart.getText().toString())) {
            Toast.makeText(context, "Veuillez renseigner tout les champs", Toast.LENGTH_SHORT).show();
            return;
        }
        if(TextUtils.isEmpty(dateEnd.getText().toString())) {
            Toast.makeText(context, "Veuillez renseigner tout les champs", Toast.LENGTH_SHORT).show();
            return;
        }
        if(TextUtils.isEmpty(duree.getText().toString())) {
            Toast.makeText(context, "Veuillez renseigner tout les champs", Toast.LENGTH_SHORT).show();
            return;
        }
        if(TextUtils.isEmpty(price.getText().toString())) {
            Toast.makeText(context, "Veuillez renseigner tout les champs", Toast.LENGTH_SHORT).show();
            return;
        }

        Event newEvent = new Event(title.getText().toString(),shortDesc.getText().toString(),desc.getText().toString(),0,dateStart.getText().toString(), dateEnd.getText().toString(), Integer.parseInt(duree.getText().toString()), Double.parseDouble(price.getText().toString()));
        newEvent.addEvent(context);
        Intent intentListPlace = new Intent(EditEventActivity.this, ListEventActivity.class);
        EditEventActivity.this.startActivity(intentListPlace);
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        Intent intentHome = new Intent(EditEventActivity.this, MainActivity.class);
        Intent intentMap = new Intent(EditEventActivity.this, MapActivity.class);
        Intent intentListPlace = new Intent(EditEventActivity.this, ListPlaceActivity.class);
        Intent intentListEvent = new Intent(EditEventActivity.this, ListEventActivity.class);

        switch (item.getItemId()) {
            case R.id.menu_home:
                EditEventActivity.this.startActivity(intentHome);
                return true;
            case R.id.menu_map:
                EditEventActivity.this.startActivity(intentMap);
                return true;
            case R.id.menu_list_place:
                EditEventActivity.this.startActivity(intentListPlace);
                return true;
            case R.id.menu_list_event:
                EditEventActivity.this.startActivity(intentListEvent);
                return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
